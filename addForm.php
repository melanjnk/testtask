<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 19.04.2015
 * Time: 2:00
 */
?>
<hr>
<form action="file-upload.php" method="post" enctype="multipart/form-data" id="formx">
    <div class="add">
        <h1>Добавление новой записи</h1>

        <div class="col">
            <div class="fieldnames">
                <label>Название:</label>
                <input type="text" name="title" id="addtitle"/>

                <div class="errorsAdd"></div>
            </div>
            <div class="fieldnames">
                <label>Пост:</label>
                <textarea rows="10" cols="45" name="text" id="addtext"></textarea>

                <div class="errorsAdd"></div>
            </div>
            <div class="fieldnames">
                <label>Картинка:</label>
                <input type="file" name="fileToUpload" id="fileToUpload">

                <div class="errorsAdd"></div>
            </div>
            <div class="fieldnames">
                <label>Капча:</label>

                <div class="g-recaptcha" data-sitekey="6LcxkAUTAAAAAMdH1rMGOp7OfXQaMlCrs4MeNgJ7"></div>
                <div class="errorsAdd"></div>
            </div>
            <div class="fieldnames">
                <label>Управление</label>
                <input type="submit" value="Upload Image" name="add_post">

                <div class="errorsAdd"></div>
            </div>
        </div>
    </div>
</form>
</div>
</body>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
</html>
