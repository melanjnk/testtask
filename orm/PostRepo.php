<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 19.04.2015
 * Time: 1:46
 */

require_once(dirname(__FILE__) . '\..\Post.php');
require_once('AbstractRepo.php');

class PostRepo extends AbstractRepo{

    /**
     * this method must return name of table in db
     * @return string
     */
    protected function getTableName()
    {
        return 'post';
    }

    /**
     * must return full list of field in table that is equal to entity
     * @return array
     */
    protected function getMapping()
    {
        return array('id','title','author','post','img');
    }

    /**
     * return new Instance of entity
     * @return object|null
     */
    protected function getInstance()
    {
        return new Post();
    }
}