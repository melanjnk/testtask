<?php

/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 18.04.2015
 * Time: 23:39
 */

require_once(dirname(__FILE__) . '\..\User.php');

require_once('AbstractRepo.php');

class UserRepo extends AbstractRepo
{

    /**
     * this method must return name of table in db
     * @return string
     */
    protected function getTableName()
    {
        return 'user';
    }

    /**
     * must return full list of field in table that is equal to entity
     * @return array
     */
    protected function getMapping()
    {
        return array('id','login','password','mail','mail_reg','salt','reg_date','last_act');
    }

    /**
     * return new Instance of entity
     * @return object|null
     */
    protected function getInstance()
    {
        return new User();
    }

}