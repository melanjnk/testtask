<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 04.12.2014
 * Time: 22:19
 */

require_once('\..\Config.php');

/**
 * light clone of Doctrine ORM (with PDO)
 * Class AbstractRepo
 * @package Orm
 */
abstract class AbstractRepo
{
    /**
     * this method must return name of table in db
     * @return string
     */
    abstract protected function getTableName();

    /**
     * must return full list of field in table that is equal to entity
     * @return array
     */
    abstract protected function getMapping();

    /**
     * return new Instance of entity
     * @return object|null
     */
    abstract protected function getInstance();

    /**
     * transform one letter after _ to uppercase
     * @param $string
     * @return mixed
     */
    private function getFieldCanonicalName($string)
    {
        while (strpos($string, '_') > 0) {
            $pos = strpos($string, '_');
            $string = substr_replace($string, '', $pos, 1);
            $letter = strtoupper(substr($string, $pos, 1));
            $string = substr_replace($string, $letter, $pos, 1);
        }
        return $string;
    }


    /**
     * @param object $arr
     * @return mixed
     */
    private function getObject($arr)
    {
        $mapping = $this->getMapping();
        $instance = $this->getInstance();
        foreach ($mapping as $columnName) {
            $method = 'set' . ucfirst($this->getFieldCanonicalName($columnName));
            if (isset($arr[$columnName])) {
                $instance->$method($arr[$columnName]);
            }
        }
        return $instance;
    }

    /**
     * @param object $obj
     * @return array
     */
    private function getArray($obj)
    {
//        $arr = array();
//        $arrObj = is_object($obj) ? get_object_vars($obj) : $obj;
//        foreach ($arrObj as $key => $val) {
//            $val = (is_array($val) || is_object($val)) ? $this->getArray($val) : $val;
//            $arr[$key] = $val;
//        }
//        return $arr;

        $mapping = $this->getMapping();
//        print_r($mapping);
        $list = array();
        foreach ($mapping as $value) {
            $method = 'get' . ucfirst($this->getFieldCanonicalName($value));
            $list[$value] = $obj->$method();
            print_r($list);
        }
        if (count($list))
        {
            print_r($list);
            return $list;
        }


        else return 0;
    }

    /**
     * @var array of errors
     */
    protected $error;

    /**
     * @return null|\PDO
     */
    protected function getConnection()
    {
        $conf = \Config::GetInstance();
        $dsn = 'mysql:host=' . $conf->getHost() . ';dbname=' . $conf->getDbname();

        $options = array(
            \PDO::ATTR_PERSISTENT => true,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
        );

        try {
            $dbh = new \PDO($dsn, $conf->getUser(), $conf->getPass(), $options);
            $dbh->exec("set names utf8");
        } catch (\PDOException $e) {
            $dbh = null;
            $this->error = $e->getMessage();
        }
        return $dbh;
    }

    /**
     * execute a custom query
     * @param $queryString
     * @return array
     */
    protected function exec($queryString)
    {
        $db = $this->getConnection();
        $stmt = $db->query($queryString);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        $list = array();
        foreach ($result as $val) {
            $list[] = $this->getObject($val);
        }
        return $list;
    }

    /**
     * fetch all selection
     * @return array
     */
    public function findAll()
    {

        $db = $this->getConnection();
        if (!$db == null) {
            $query = 'SELECT * FROM ' . $this->getTableName();
            $stmt = $db->query($query);
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);
            $result = $stmt->fetchAll();
            $list = array();
            foreach ($result as $value) {
                $list[] = $this->getObject($value);
            }
            return $list;
        }
        return array();
    }

    /**
     * selected element by id
     * @param int $id
     * @return mixed
     */
    public function find($id)
    {
        $db = $this->getConnection();
        $query = 'SELECT * FROM ' . $this->getTableName() . ' WHERE id=' . intval($id);
        $stmt = $db->query($query);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = array($stmt->fetch());
        return (count($result) > 0) ? $this->getObject($result[0]) : null;
    }

    /**
     * add new element
     * @param object $obj
     * @return string
     */
    public function add($obj)
    {
        $db = $this->getConnection();

        $obj = $this->getArray($obj);

        $keyList = implode(", ", array_keys($obj));
        $valList = array_values($obj);
        foreach ($valList as &$value) {
            $value = '"' . $value . '"';
        }
        $valList = implode(", ", $valList);
        $query = 'INSERT INTO ' . $this->getTableName() . ' (' . $keyList . ') VALUES (' . $valList . ');';
        $db->query($query);
        return $db->lastInsertId();
    }

    /**
     * update element
     * @param object $obj
     */
    public function update($obj)
    {
        $db = $this->getConnection();

        $assignment = array();

        $obj = $this->getArray($obj);
        foreach ($obj as $key => $value) {
            $assignment[] = $key . '="' . $value . '"';
        }
        $assignment = implode(", ", $assignment);
        $query = 'UPDATE ' . $this->getTableName() . ' SET ' . $assignment . ' WHERE id=' . $obj["id"];
        $db->query($query);
    }

    /**
     * delete element by id
     * @param int $id
     */
    public function delete($id)
    {
        $db = $this->getConnection();
        try {
            $query = 'DELETE FROM ' . $this->getTableName() . ' WHERE id=' . $id;
            $db->exec($query);
        } catch (\PDOException $e) {
            $e->getMessage();
        }
    }

    /**
     * save changes current element
     * @param object $obj
     */
    public function save($obj)
    {
        if ($obj["id"] == 0 || !isset($obj["id"])) $this->add($obj);
        else $this->update($obj);
    }

    /**
     * select element by criterion
     * @param array $criteria
     * @return array
     */
    public function findBy($criteria=array(),$page=0,$limit=0)
    {
        $db = $this->getConnection();
        if (!$db == null) {
            $criteriaList = array();

            foreach ($criteria as $key => $value) {
                $criteriaList[] = $key . '="' . $value . '"';
            }
            $limdown = ''.$limit*($page-1);
            $limup = ''.$limit;

            $query = 'SELECT * FROM ' . $this->getTableName() ;
                if(count($criteria))
                    $query .= ' WHERE ' . implode(" AND ", $criteriaList);
                if($page!=0)
                    $query .= 'LIMIT ' . $limdown . ',' . $limup;;

            $stmt = $db->query($query);
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);
            $result = $stmt->fetchAll();
            $list = array();
            foreach ($result as $value) {
                $list[] = $this->getObject($value);
            }
            return $list;
        }
        else
            return array();
    }

    public function getCount(){
        $db = $this->getConnection();
        if (!$db == null) {
            $query = 'select count(*) as count_row FROM '. $this->getTableName();
            $stmt = $db->query($query);
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);
            $result = $stmt->fetchAll();
            $result = $result[0]['count_row'];
            return $result;
        }
        return 0;
    }
}