<?php

/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 18.04.2015
 * Time: 23:34
 */
class User
{
    private $id;
    private $login;
    private $password;
    private $salt;
    private $mail;
    private $mail_reg;
    private $last_act;
    private $reg_date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getLastAct()
    {
        return $this->last_act;
    }

    /**
     * @param mixed $last_act
     */
    public function setLastAct($last_act)
    {
        $this->last_act = $last_act;
    }

    /**
     * @return mixed
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @param mixed $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return mixed
     */
    public function getMailReg()
    {
        return $this->mail_reg;
    }

    /**
     * @param mixed $mail_reg
     */
    public function setMailReg($mail_reg)
    {
        $this->mail_reg = $mail_reg;
    }
} 