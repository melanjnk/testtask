-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 19 2015 г., 18:25
-- Версия сервера: 5.6.17
-- Версия PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` smallint(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET cp1251 DEFAULT NULL,
  `author` smallint(8) NOT NULL,
  `post` varchar(500) CHARACTER SET cp1251 DEFAULT NULL,
  `img` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `title`, `author`, `post`, `img`) VALUES
(1, 'Car of my Dream', 1, 'Most interesting car for me is Ferrary', 'ferrary.jpg'),
(2, 'Plane of the weak', 1, 'Boing 747 is reasinable choise for travelling', 'boing.jpg'),
(4, 'My country', 0, 'best ever town what i see', '3_2.png'),
(5, 'last hours', 1, 'great season was one', '3_2.png'),
(20, 'MarkisGavvana', 1, 'Датой открытия сети обувных магазинов M?RKIS можно считать 19 апреля 2010 года, когда сразу в пяти торговых точках в городе Калининграде был презентован новый концепт обувных магазинов с новым названием M?RKIS. Новый формат стал результатом проведенного ребрендинга сети магазинов «Мир Обуви», работающих в Калининграде с 2006 года и ставших за это время одними из любимейших обувных магазинов жителей города и области. Изменения коснулись не только названия сети магазинов, но и многих других важных', 'shop.png'),
(21, 'My country', 1, 'Japan go to heven', '3_211.png'),
(22, 'My country jlk', 1, 'djkghl sjhfl ', '3_211.png'),
(23, 'kgj', 1, 'dfkljhkld', '3_211.png'),
(24, '', 1, '', ''),
(25, 'google-cap', 1, 'вывыпып', '3_211.png'),
(26, 'vas9', 1, 'jdlgkjlg', '3_211.png'),
(27, 'ddsfh', 1, 'hhdghjd', '3_211.png'),
(28, 'sdfsdf', 1, 'sdfsd', '3_211.png'),
(29, 'ya', 1, 'ththfh', '3_211.png');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(25) CHARACTER SET cp1251 NOT NULL,
  `password` varchar(32) CHARACTER SET cp1251 NOT NULL,
  `salt` varchar(3) CHARACTER SET cp1251 NOT NULL,
  `mail_reg` varchar(50) CHARACTER SET cp1251 NOT NULL,
  `mail` varchar(50) CHARACTER SET cp1251 NOT NULL,
  `last_act` int(11) NOT NULL,
  `reg_date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `salt`, `mail_reg`, `mail`, `last_act`, `reg_date`) VALUES
(1, 'melanj.ks.ua@gmail.com', '1234bob', 'and', 'melanj.ks.ua@gmail.com', 'melanj.ks.ua@gmail.com', 0, 0),
(2, 'mom@mail.ru', 'qwer1234', 'lol', 'mom@mail.ru', 'mom@mail.ru', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
