<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 18.04.2015
 * Time: 18:21
 */
require_once('orm/UserRepo.php');
require_once('User.php');
require_once('orm/PostRepo.php');
require_once('Post.php');
include('auth-form.php');
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="/js/app.js"></script>
    <link href="css/styles.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class="wrapper">

    <?php
    $login = 'guest';
    if (isset($_POST['login']))
        $login = $_POST['login'];
    if (isset($_POST['password']))
        $password = $_POST['password'];

    if (isset($_SESSION['user_id'])) {
        $id = $_SESSION['user_id'];
        $userRepo = new UserRepo();
        $user = $userRepo->find($id);
        $login = $user->getLogin();


        echo 'hello ' . $login;
        echo '<br><a href="/?action=out">Quit</a>';
        ?>
        <?php
        $postRepo = new PostRepo();

        $countPosts = $postRepo->getCount();
        $limitpages = 5;
        $pages = floor($countPosts / $limitpages) + (($countPosts % $limitpages > 0) ? 1 : 0);

        echo '<br> Количество страниц : ' . $pages;
        ?>
        <div id="nav">
            <p>выберите страницу:</p>
            <?php

            echo '<br>';
            for ($i = 1; $i <= $pages; $i++) {
                ?>
                <a class="linktopage" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a>
            <?php
            }
            ?>


        </div>
        <?php

        $arr = array(
            "author" => $_SESSION['user_id'],
        );

        if (isset($_GET['page'])) {
            $posts = $postRepo->findBy($arr, $_GET['page'], 5);
        } else {
            $posts = $postRepo->findBy($arr, 1, 5);
        }
        foreach ($posts as $post) {
            ?>
            <div class="record">
                <img width="128" height="128" src="/uploads/<?php echo $post->getImg() ?>">

                <div class="articleTitle">
                    <!--            <p>--><?php //echo $post->getId(); ?><!--</p>-->

                    <p><?php echo $post->getTitle(); ?></p>
                </div>
                <div class="articleText">
                    <!--            <p>--><?php //echo $post->getAuthor(); ?><!--</p>-->

                    <p><?php echo $post->getPost(); ?></p>
                </div>
            </div>
            <div class="clear"></div>
        <?php
        }
        include_once('addForm.php');
    }
    ?>

